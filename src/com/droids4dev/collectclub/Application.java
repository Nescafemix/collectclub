package com.droids4dev.collectclub;

import com.droids4dev.collectclub.model.Collection;
import com.droids4dev.collectclub.model.CollectionFactory;


public class Application extends android.app.Application {
	private static Application application = null;
	private static Collection[] myCollections = null;
	private static Collection[] otherCollections = null;
	
	@Override
	public void onCreate() {
		super.onCreate();
		application = this;
	}

	public static Application getInstance() {
		return application;
	}
	
	public Collection[] getMyCollections() {
		if (myCollections == null) {
			myCollections = CollectionFactory.getMyRandomCollections(11);
		}
		return myCollections;
	}
	
	public Collection[] getOtherCollections() {
		if (otherCollections == null) {
			otherCollections = CollectionFactory.getRandomCollectionArray(15);
		}
		return otherCollections;
	}

}
