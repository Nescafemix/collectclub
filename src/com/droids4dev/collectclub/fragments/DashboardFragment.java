package com.droids4dev.collectclub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.LinearLayout;

import com.droids4dev.collectclub.R;
import com.droids4dev.collectclub.mycollection.MyItemsFragment;

public class DashboardFragment extends Fragment{
    public static final String ARG_SECTION_NUMBER = "section_number";
    public static final String COLLECTION_FRAGMENT_TAG = "CollectionFragment";

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        String section = getResources().getString(R.string.app_name);

        getActivity().setTitle(section);
        
        
        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.collection_android);
        
        linearLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FragmentManager fragmentManager = getFragmentManager();
				MyItemsFragment fragment = new MyItemsFragment();
				Bundle arguments = new Bundle();
				arguments.putInt(MyItemsFragment.COLLECTION_POSITION, 0);
				fragment.setArguments(arguments);
				fragmentManager
					.beginTransaction()
						.replace(R.id.content_frame, fragment, COLLECTION_FRAGMENT_TAG)
						.commit();				
				
				
			}
		});
        
        
        
        
        return rootView;
    }
}
