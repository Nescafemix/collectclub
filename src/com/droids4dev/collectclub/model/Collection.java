package com.droids4dev.collectclub.model;

public class Collection {
	public String name;
	public String description;
	public Item[] items;
	public String imagePath;
	
	public int getSize() {
		return items.length;
	}
	
	public Item getItem(int i) {
		return items[i];
	}
}
