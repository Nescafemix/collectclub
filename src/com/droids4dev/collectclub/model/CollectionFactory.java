package com.droids4dev.collectclub.model;

import java.util.Random;

public class CollectionFactory {
	private static final String[] OWNER_NAME = {"Fermin", "Marvin", "Bob", "Juan", "Paul", "Siri"};
	private static final int OWNER_LENGTH = OWNER_NAME.length;
	private static final String DESCRIPTION = "This collection was completed thanks to ";
	private static final Random RANDOM = new Random();
	
	public static Collection getRandomCollection(String itemType, int collectionSize, boolean isMine) {
		Collection collection = new Collection();
		if (!isMine) {
			collection.name = OWNER_NAME[getRandomInt(OWNER_LENGTH)] + "'s " + itemType + " Collection"; 
		} else {
			collection.name = itemType + " Collection";
		}
		collection.description = DESCRIPTION + OWNER_NAME[getRandomInt(OWNER_LENGTH)];
		collection.items = ItemFactory.getRandomItemArray(itemType, collectionSize);
		return collection;
	}
	
	public static Collection getRandomCollection(String itemType, int collectionSize) {
		return getRandomCollection(itemType, collectionSize, false);
	}
	
	public static Collection[] getRandomCollectionArray(int arraySize) {
		Collection[] collectionArray = new Collection[arraySize];
		for (int i=0; i<arraySize; i++) {
			collectionArray[i] = getRandomCollection(ItemFactory.getRandomType(), getRandomInt(arraySize));
		}
		return collectionArray;
	}
	
	public static Collection[] getMyRandomCollections(int arraySize) {
		Collection[] collectionArray = new Collection[ItemFactory.TYPE_LENGTH];
		for (int i=0; i<ItemFactory.TYPE_LENGTH; i++) {
			collectionArray[i] = getRandomCollection(ItemFactory.TYPE_ARRAY[i], getRandomInt(arraySize), true);
		}
		return collectionArray;
	}
	
	private static int getRandomInt(int limit) {
		int value = RANDOM.nextInt(limit);
		if (value == 0) {
			value = 1;
		}
		return value;
	}

}
