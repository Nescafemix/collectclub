package com.droids4dev.collectclub.model;

public class Item {
	public int id;
	public String name;
	public boolean owned;
	public ItemStatus status;
	public int imageId;
}
