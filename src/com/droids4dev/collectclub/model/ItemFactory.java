package com.droids4dev.collectclub.model;

import java.util.Random;

import com.droids4dev.collectclub.R;


public class ItemFactory {
	private static final String[] ADJECTIVE_ARRAY = {"Very rare","Rare", "Special Edition","Secret", "Vintage", "First edition", "Author signed"};
	private static final int ADJECTIVE_LENGTH = ADJECTIVE_ARRAY.length;
	private static final ItemStatus[] STATUS_ARRAY = ItemStatus.values();
	private static final int STATUS_LENGTH = STATUS_ARRAY.length;
	
	public static interface Type {
		public static final String ANDROID = "Android";
		public static final String COIN = "Coin";
		public static final String STICKER = "Sticker"; 
	}
	
	public static final String[] TYPE_ARRAY = {Type.ANDROID, Type.COIN, Type.STICKER};
	public static final int TYPE_LENGTH = TYPE_ARRAY.length;
	
	private static final Random RANDOM = new Random();
	
	public static Item[] getRandomItemArray(String type, int size) {
		Item[] items = new Item[size];
		for (int i=0; i<size; i++) {
			items[i] = getRandomItem(type);
		}
		return items;
	}
	
	public static Item getRandomItem(String type) {
		Item item = getItem(type);
		return item;
	}
	
	public static String getRandomType() {
		return TYPE_ARRAY[getRandomInt(TYPE_LENGTH)];
	}

	private static Item getItem(String type) {
		Item item = new Item();
		item.name = getRandomName(type);
		item.owned = getRandomBoolean();
		item.status = getItemStatus(item.owned);
		item.imageId = R.drawable.ic_launcher;
		return item;
	}

	private static ItemStatus getItemStatus(boolean owned) {
		if (owned) {
			return getRandomStatus();
		}
		return ItemStatus.NONE;
	}

	private static String getRandomName(String type) {
		return ADJECTIVE_ARRAY[getRandomInt(ADJECTIVE_LENGTH)] + " " + type;
	}
	
	private static ItemStatus getRandomStatus() {
		return STATUS_ARRAY[getRandomInt(STATUS_LENGTH)];
	}
	
	private static boolean getRandomBoolean() {
		return RANDOM.nextBoolean();
	}
	
	private static int getRandomInt(int limit) {
		return RANDOM.nextInt(limit);
	}

}
