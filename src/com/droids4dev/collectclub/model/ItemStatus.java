package com.droids4dev.collectclub.model;

public enum ItemStatus {
	NONE,
	DESIRED,
	SELLING,
	CHANGING
}
