package com.droids4dev.collectclub.mycollection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.droids4dev.collectclub.R;
import com.droids4dev.collectclub.model.Collection;
import com.droids4dev.collectclub.model.Item;

public class ItemsAdapter extends BaseAdapter{
	private final Collection collection;
	private final LayoutInflater inflater;
		
	public ItemsAdapter(Context context, Collection collection) {
		this.collection = collection;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return collection.getSize();
	}

	@Override
	public Object getItem(int position) {
		return collection.getItem(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = inflater.inflate(R.layout.item, parent, false);
		}
		
		Item item = collection.getItem(position);
		
		TextView nameTextView = (TextView) view.findViewById(R.id.item_name);
		String name = item.name;
		nameTextView.setText(name);
		
		TextView statusTextView = (TextView) view.findViewById(R.id.item_status);
		String status = item.status.name();
		statusTextView.setText(status);
		
		ImageView imageView = (ImageView) view.findViewById(R.id.item_image);
		int resId = item.imageId;
		imageView.setImageResource(resId);
		
		return view;
	}



	
}
