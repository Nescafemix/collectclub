package com.droids4dev.collectclub.mycollection;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.widget.ListAdapter;

import com.droids4dev.collectclub.Application;
import com.droids4dev.collectclub.model.Collection;

public class MyItemsFragment extends ListFragment {
	public static final String COLLECTION_POSITION = "collectionPosition";
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		Bundle arguments = getArguments();
		int collectionPosition = arguments.getInt(COLLECTION_POSITION);
		Collection collection = Application.getInstance().getMyCollections()[collectionPosition];
		
		ListAdapter adapter = new ItemsAdapter(getActivity(), collection);
		setListAdapter(adapter);
	}	
}
